Source: regina-rexx
Section: libs
Priority: optional
Maintainer: Alen Zekulic <alen@nms.hr>
Build-Depends: libncurses5-dev,
	       debhelper-compat (=12)
Standards-Version: 4.4.1
Rules-Requires-Root: no
Homepage: http://regina-rexx.sourceforge.net/
Vcs-Git: https://salsa.debian.org/debian/regina-rexx.git
Vcs-Browser: https://salsa.debian.org/debian/regina-rexx

Package: libregina3
Architecture: any
Depends: ${shlibs:Depends},
	 ${misc:Depends}
Conflicts: regina3
Replaces: regina3
Description: Regina REXX interpreter, run-time library
 Regina is an ANSI compliant REXX interpreter for multiple platforms.
 .
 REXX is a procedural language that allows programs and algorithms
 to be written in a clear and structured way, it is also designed
 to be used as a macro language by arbitrary application programs.
 .
 Contains runtime shared libraries.

Package: libregina3-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends},
	 libregina3 (= ${binary:Version}),
	 libc6-dev,
	 cpp
Conflicts: regina2-dev,
	   regina3-dev
Replaces: regina2-dev,
	  regina3-dev
Description: Regina REXX interpreter, development files
 Regina is an ANSI compliant REXX interpreter for multiple platforms.
 .
 REXX is a procedural language that allows programs and algorithms
 to be written in a clear and structured way, it is also designed
 to be used as a macro language by arbitrary application programs.
 .
 Contains static library, header file rexxsaa.h and regina-config
 script.

Package: regina-rexx
Section: interpreters
Architecture: any
Depends: ${shlibs:Depends},
	 ${misc:Depends}
Description: Regina REXX interpreter
 Regina is an ANSI compliant REXX interpreter for multiple platforms.
 .
 REXX is a procedural language that allows programs and algorithms
 to be written in a clear and structured way, it is also designed
 to be used as a macro language by arbitrary application programs.
 .
 Contains the Regina REXX interpreter (regina and rexx), external
 queue support (rxstack and rxqueue) and example programs.
